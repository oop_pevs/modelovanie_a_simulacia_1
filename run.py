import random, time
from operator import itemgetter
from datetime import datetime

class Example1():
    simtime = int(time.time()) # current timestep
    maxtime = simtime + 600 # maximal simulation timesteps
    calendar = [] # array [(time,event), (time,event)]
    persons = 0 # number of persons in the hall

    timeLog = ["cas"]
    hallLog = ["ludia"]

    '''
        Add (simtime+delay, event) to calendar
    '''
    def event_schedule(self, delay, event):
        self.calendar.append((self.simtime + delay, event))
        self.calendar.sort(reverse=True, key=itemgetter(0)) # Reverse sort calendar by time after each append

    '''
        Get first (time, event) from calendar
        In my case it is last because it is sorted reverse
    '''
    def event_cause(self):
        event = self.calendar.pop()
        self.simtime = event[0] # set current simulation time
        return event

    '''
        people are coming to the hall
    '''
    def event1(self):
        i = random.randint(1, 3)
        print "persons came: %d" % (i)
        self.persons += i
        self.event_schedule(random.randint(1, 2), 1)

    '''
        Elevator 1
        lift departures from the hall
    '''
    def event2(self):
        self.persons -= 3
        if self.persons < 0:
            self.persons = 0

        print "lift left, waiting persons: %s" % (self.persons)
        self.event_schedule(random.randint(2, 5), 2)

    def createChart(self):
        f = open("file.csv","w+")
        # First row in table
        for people in self.hallLog:
            f.write(str(people) + ",")
        f.write("\n")

        # Second row in table
        for time in self.timeLog:
            f.write(str(time) + ",")
        f.write("\n")
        f.close()

    def run(self):
        print "simulation start\n"
        self.event_schedule(1, 1)
        self.event_schedule(1, 2)
        while self.simtime < self.maxtime:
            event = self.event_cause()[1]
            print "\ntime: %s  event: %s  " % (datetime.fromtimestamp(self.simtime).isoformat(), event)
            if event == 0: break
            if event == 1: self.event1()
            if event == 2: self.event2()

            self.hallLog.append(self.persons)
            self.timeLog.append(datetime.fromtimestamp(self.simtime).isoformat())

        self.createChart()
        print "\n\nsimulation finished\n\n"


a = Example1()
a.run()